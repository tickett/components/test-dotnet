# .NET Test

.NET Test will run tests for a .NET project and display and vizualize code coverage in GitLab.

![Code Coverage Visualization](https://gitlab.com/tickett/components/dotnet-test/-/raw/main/images/coverage_visualization.png)

![Code Coverage Percentage](https://gitlab.com/tickett/components/dotnet-test/-/raw/main/images/coverage_percentage.png)

## Usage

```yml
include:
  - component: gitlab.com/tickett/components/dotnet-test/dotnet-test@main
    inputs:
      image: mcr.microsoft.com/dotnet/sdk:6.0
      stage: test
      test_project_folder: ServiceTests
      test_project_name: ServiceTests
```

## Inputs

### `image`

Default: `mcr.microsoft.com/dotnet/core/sdk:latest`

The .NET Docker image.

### `stage`

Default: `test`

The pipeline stage in which to place the test job.

### `test_project_folder`

Default value: `Tests`

The relative path to the folder containing the test project.

### `test_project_name`

Default: `Tests`

The name of the test project.

### `nuget_config`

A file containing custom nuget configuration.

### `test_logger`

Default: `junit`

The test logger that the test project uses for reporting.
Common options include `junit`, `nunit`, `xunit` or other compatible formats.

### `tag`

A tag to be used for the job (to control where the job is executed).
