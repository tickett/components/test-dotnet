using ConsoleApp;
using Xunit;

namespace Tests
{
    public class UnitTest
    {
        [Fact]
        public void AddTest()
        {
            var result = new ProgramService().Add(5, 10);

            Assert.Equal(15, result);
        }

        [Fact]
        public void SubtractTest()
        {
            var result = new ProgramService().Subtract(25, 15);

            Assert.Equal(10, result);
        }
    }
}